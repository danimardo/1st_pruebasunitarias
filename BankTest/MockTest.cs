﻿using System;
//using Microsoft.VisualStudio.TestTools.UnitTesting;
using Bank;
using NUnit.Framework;
using Rhino.Mocks;

namespace BankTest
{
    class MockTest
    {
       

        [Test]
        // con Mock
        /* Un Mock es idéntico a un Stub, pero con el añadido que en la creación del Mock, se añaden condiciones o “matchers” para comprobar la correcta utilización
         de la clase. Aportando así más pruebas al test.
         Los Mocks, “son objetos preprogramados con expectativas que conforman la especificación de lo que se espera que reciban las llamadas”, es decir, 
         son objetos que se usan para probar que se realizan correctamente llamadas a otros métodos, por ejemplo, a una web API, por lo que se utilizan para verificar
         el comportamiento de los objetos. 
         Osea, Con el Mock probaremos que el método se hace la llamada al servicio correctamente; y con el Stub probaremos que el resultado es el esperado.
            */
        public void OrchestratorCallsA()
        {
            ITarjetaDeCredito tarjetaMock =
             MockRepository.GenerateStrictMock<ITarjetaDeCredito>();

            // Lo que hace esto es definir un objeto ITarjetaDeCredito que cuando lo ponga como argumento de un metodo o constructor
            // en realidad no hará nada y devolverá el valor que indiquemos. Esto es así porque en realidad el objeto tarjetaMock cumple la interface pero
            // no tiene implementación.
            // Osea, que el mock va a esperar si algún metodo del interface se ejecuta y si cumple con lo que nosotros esperamos que cumpla.
            // El argumento que pasamos lo comprobará para saber si más adelante lo llamará con los argumentos que nosotros ponemos o con otros.
            // Si se llama con otros argumentos no se cumplirá lo esperado y tendremos un test en rojo.
            // Como dije antes, el return que ponemos en el lambda es lo que obtendremos ya que en realidad el metodo es un dummy y no ejecutará nada. Osea, que lo indicamos nosotros.
            // Las expectativas pueden ser una o podemos poner todas las que queramos. Pero teniendo en cuenta que han de ir en orden. Si las expectativas se cumplen en 
            // diferente orden de como lo hemos indicado, nos dará un rojo.

            tarjetaMock.Expect(
             a => a.getNumeroTarjetasDeCredito(123)).Return(0);

            

            OperacionesConCuentas operadorCuentas = new OperacionesConCuentas(tarjetaMock);
            operadorCuentas.BajaTarjetas(new string[] { "8881234567890123456", "6764569360123931" }); // Aqui dentro se ejecutará el metodo getNumeroTarjetasDeCredito (SUT)

            tarjetaMock.VerifyAllExpectations();

        }


        // Con stub
        // Si se produce una llamada a X entonces devuelve Y
        /* Los Stubs, “proporcionan respuestas predefinidas a ciertas llamadas durante los test, sin responder a otra cosa para la que no hayan sido programados”,
        es decir, los stubs son configurados para que devuelvan valores que se ajusten a lo que la prueba unitaria quiere probar, por lo que se utilizan para
        verificar el estado de los objetos.
        Con el Mock probaremos que el método se hace la llamada al servicio correctamente; y con el Stub probaremos que el resultado es el esperado.
        */

        [Test]
        public void OrchestratorCallsAConStub()
        {
            ITarjetaDeCredito tarjetaStub2 = MockRepository.GenerateStub<ITarjetaDeCredito>();
            tarjetaStub2.Stub(
             x => x.getNumeroTarjetasDeCredito(123)).Return(10000);  // Si más tarde se llama a getNumeroTarjetasDeCredito con un argumento diferente
                                                                    // no retirna lo que pongamos en Return si no, que retorna 0. (no se por qu´´e)

            OperacionesConCuentas operadorCuentas2 = new OperacionesConCuentas(tarjetaStub2);
           int exito = operadorCuentas2.BajaTarjetas(new string[] { "8881234567890123456", "6764569360123931" }); // Aqui dentro se ejecutará el metodo getNumeroTarjetasDeCredito (SUT)
            // la diferencia respecto al mock es que daría igual que no se llame al metodo getNumeroTarjetasDeCredito ya que no hay Expectations.
           

            Assert.AreEqual("8881234567890123456", operadorCuentas2.tarjetas[0]);  // comprobamos que los datos del objeto son lo que esperamos
            Assert.AreEqual(900, exito); // comprobamos que el valor devuelto es el que esperamos.
                                          
        }



        // Tambien podemos crear hibridos entre uno y otro. En vez de GenerateStrictMock usaremos GenerateMock
        [Test]
        public void OrchestratorCallsHibridoMoksYStubs()
        {
            ITarjetaDeCredito tarjetaMock3 =
             MockRepository.GenerateMock<ITarjetaDeCredito>();

            tarjetaMock3.Stub(
             x => x.HacerCosasVarias()).Return(2000); // si esta linea no estuviese y utilizasemos GenerateStrictMock en vez de GenerateStrictMock 
                                                       // y en el SUT se hiciese la llamada a hacerCosasVarias, tendríamos un rojo 
            tarjetaMock3.Expect(
               a => a.getNumeroTarjetasDeCredito(123)).Return(0);
           

            OperacionesConCuentas operadorCuentas = new OperacionesConCuentas(tarjetaMock3);
            operadorCuentas.BajaTarjetas(new string[] { "8881234567890123456", "6764569360123931" }); // Aqui dentro se ejecutará el metodo getNumeroTarjetasDeCredito (SUT)

            tarjetaMock3.VerifyAllExpectations();

        }
    }
}
