﻿using System;
//using Microsoft.VisualStudio.TestTools.UnitTesting;
using Bank;
using NUnit.Framework;


namespace BankTest
{
    [TestFixture]
    public class BankAccountTests
    {

        double beginningBalance, debitAmount, expected;


        [SetUp]
        public void SetUp()
        {
            double beginningBalance;

        }


        // unit test code
        [Test]
        public void Debit_WithValidAmount_UpdatesBalance()
        {
            // arrange
            beginningBalance = 11.99;
            debitAmount = 4.55;
            expected = 7.44;
            BankAccount account = new BankAccount("Mr. Bryan Walton", beginningBalance);

            // act
            account.Debit(debitAmount);

            // assert
            double actual = account.Balance;
            Assert.AreEqual(expected, actual, 0.001, "Account not debited correctly");
        }

               
        [Test]
        [ExpectedException("System.ArgumentOutOfRangeException")]
        public void Debit_WhenAmountIsLessThanZero_ShouldThrowArgumentOutOfRange()
        {
            // arrange
            beginningBalance = 11.99;
            debitAmount = -100.00;
            BankAccount account = new BankAccount("Mr. Bryan Walton", beginningBalance);

            // act
            account.Debit(debitAmount);

            // assert is handled by ExpectedException
        }

    
        [Test]
        public void Debit_WhenAmountIsMoreThanBalance_ShouldThrowArgumentOutOfRange()
        {
            // arrange
            beginningBalance = 11000.99;
            debitAmount = 20.0;
            BankAccount account = new BankAccount("Mr. Bryan Walton", beginningBalance);

            // act
            try
            {
                account.Debit(debitAmount);
            }
            catch (ArgumentOutOfRangeException e)
            {
                // assert
                StringAssert.Contains(e.Message, BankAccount.DebitAmountExceedsBalanceMessage);
                return;
            }
            //Assert.Fail("No exception was thrown.");
        }
   

    }
}
