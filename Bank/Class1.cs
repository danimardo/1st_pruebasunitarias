﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Bank
{
    public interface ITarjetaDeCredito {

         int getNumeroTarjetasDeCredito(int codCliente);

        int HacerCosasVarias();

    }
        /// <summary> 
        /// Bank Account demo class. 
        /// </summary> 
        public class BankAccount : ITarjetaDeCredito
    {
            private string m_customerName;

            private double m_balance;

            private bool m_frozen = false;

            public const string DebitAmountExceedsBalanceMessage = "Debit amount exceeds balance";
            public const string DebitAmountLessThanZeroMessage = "Debit amount less than zero";

        public BankAccount()
            {
            }

            public BankAccount(string customerName, double balance)
            {
                m_customerName = customerName;
                m_balance = balance;
            }

            public string CustomerName
            {
                get { return m_customerName; }
            }

            public double Balance
            {
                get { return m_balance; }
            }

            public void Debit(double amount)
            {
                if (m_frozen)
                {
                    throw new Exception("Account frozen");
                }

                if (amount > m_balance)
                {
                    throw new ArgumentOutOfRangeException("amount", amount, DebitAmountExceedsBalanceMessage);
                }

                if (amount < 0)
                {
                    throw new ArgumentOutOfRangeException("amount", amount, DebitAmountLessThanZeroMessage);
                }

            m_balance -= amount; 
            }

            public void Credit(double amount)
            {
                if (m_frozen)
                {
                    throw new Exception("Account frozen");
                }

                if (amount < 0)
                {
                    throw new ArgumentOutOfRangeException("amount");
                }

                m_balance += amount;
            }

            private void FreezeAccount()
            {
                m_frozen = true;
            }

            private void UnfreezeAccount()
            {
                m_frozen = false;
            }

            public static void Main()
            {
                BankAccount ba = new BankAccount("Mr. Bryan Walton", 11.99);

                ba.Credit(5.77);
                ba.Debit(11.22);
                Console.WriteLine("Current balance is ${0}", ba.Balance);

            }

        public int getNumeroTarjetasDeCredito(int codCliente)
        {
            // Supuestamente me conecto a la base y lo devuelvo. 
            // No lo voy a implementar porque no viene a cuento para esta prueba.
            System.Console.WriteLine("Ejecutando el SUT con int codCliente: {0}",codCliente);

            // ejecutamos un comando de shell para comprobar si se ejecuta o por el contrario no se está ejecutando
           
            Process proc = new Process
            {
                StartInfo = new ProcessStartInfo
                {
                    FileName = "c:\\temp\\timeT.cmd",
                    Arguments = "",
                    UseShellExecute = false,
                    RedirectStandardOutput = true,
                    CreateNoWindow = true
                }
            };
            proc.Start();


            return 5;
        }

        public int HacerCosasVarias()
        {
            System.Console.WriteLine("Hago cosas varias");
            return 40;
        }

    }
    }


