﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Bank;

namespace Ejecutable
{
    class Program
    {
        static void Main(string[] args)
        {
            BankAccount cuentaDeMiBanco = new BankAccount();
            OperacionesConCuentas operadorCuentas = new OperacionesConCuentas(cuentaDeMiBanco );
            operadorCuentas.BajaTarjetas(new string[] { "1234567890123456", "6764569360123931" });
            System.Console.WriteLine("Hello, World!");

        }
    }
}
